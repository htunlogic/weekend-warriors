import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/**
 * Route components.
 */
import About from '../components/About.vue';
import Trails from '../components/Trails.vue';
import Enduro from '../components/Enduro.vue';
import Contact from '../components/Contact.vue';
import Humanitarian from '../components/Humanitarian.vue';

/**
 * Router is defined and fed with routes.
 */
export default new Router({
  routes: [
    {
      name: "About",
      path: "/about",
      component: About
    },
    {
      name: "Enduro",
      path: "/enduro",
      component: Enduro
    },
    {
      name: "Humanitarian",
      path: "/humanitarian",
      component: Humanitarian
    },
    {
      name: "Trails",
      path: "/trail",
      component: Trails
    },
    {
      name: "Contact",
      path: "/contact",
      component: Contact
    }
  ]
});
